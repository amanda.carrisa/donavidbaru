const myUrl = "https://donavidbaru.herokuapp.com"

const formatsearchds = (searchds) => {
        $("#sp_result").append(
            `
            <div class="col-lg-4 col-sm-6 mb-4">
                <div class="card">
                    <img class="card-img-top" src='/${searchds.fields.photo}'>
                    <div class="card-body">
                        <h5 class="card-title">${ searchds.fields.name }</h5>
                        <p class="card-text">Sisa hari: ${ searchds.fields.remDay } hari lagi <br> Target : Rp.${ searchds.fields.target }</p>
                        <div class="center">
                            <a href="/formulir_donasi/${searchds.pk}" class="btn rounded-pill btn-donasi"
                                style="">Donasi</a>
                            <a href="/donasi-sosial/${searchds.pk}" class="btn rounded-pill btn-donasi"
                                style="">Lihat Donatur</a>
                        </div>
                    </div>
                </div>
            </div>
            `
        );
}

const formatSemuasearchds = (data) => {  
    data.forEach(function(searchds, index){
        formatsearchds(searchds)
    })
}
const formatSearchRecommendation = (data, key) => {
    data.forEach(function(searchds, index){
        if(searchds.fields.name.toLowerCase().includes(key.toLowerCase()) ){
            formatsearchds(searchds);
        }  
    })
}

$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url: `/donasi-sosial/get/data/`,
        success: function(res){
            $("#sp_result").empty();
            formatSemuasearchds(res)
        }
    })

    $('#search_submit').on('click', function(){
        let key = $("#search_input").val();
        if(key != ""){
            $.ajax({
                method: 'GET',
                url: `/donasi-sosial/get/data/`,
                success: function(res){
                    $("#sp_result").empty();
                    formatSearchRecommendation(res, key)
                }
            })
        }
    })

    $("#search_input").keyup(function () {
        let key = $("#search_input").val();
        if(key != ""){
            $.ajax({
                method: 'GET',
                url: `/donasi-sosial/get/data/`,
                success: function(res){
                    $("#sp_result").empty();
                    formatSearchRecommendation(res, key)
                }
            })
        }
    })

    $("#search_input").keydown(function () {
        let key = $("#search_input").val();
        if(key != ""){
            $.ajax({
                method: 'GET',
                url: `/donasi-sosial/get/data/`,
                success: function(res){
                    $("#sp_result").empty();
                    formatSearchRecommendation(res, key)
                }
            })
        }
    })

})