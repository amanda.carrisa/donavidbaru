from django.apps import AppConfig


class DonasiSosialConfig(AppConfig):
    name = 'donasi_sosial'
