from django.contrib import admin
from django.urls import path
from . import views 

app_name = 'programs'

urlpatterns = [
    path('', views.list_program, name='list_program'),
    path('<str:program>/', views.list_donatur, name='list_donatur'),
    path('get/data/', views.getData, name="getData"),
]