from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Program
from .views import list_program, getData
import tempfile
import unittest

# Create your tests here.
class Donasi_Test(TestCase):
    def test_donasi_url_is_exist(self):
        response = Client().get('/donasi-sosial/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_using_to_do_list_template(self):
        response = Client().get('/donasi-sosial/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_donasi_using_pendaftaran_func(self):
        found= resolve('/donasi-sosial/')
        self.assertEqual(found.func, list_program)

    def test_model_can_create_new_program(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Program.objects.create(
        name = "Donasi covid",
        photo = image,
        desc ="Bantu indonesia melawan covid!",
        target = 2000000,
        remDay = 44)
        counting_all_programs = Program.objects.all().count()
        self.assertEqual(counting_all_programs, 1)
    
    def test_get_data_url_is_exist(self):
        response = Client().get('/donasi-sosial/get/data/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_using_get_func(self):
        found= resolve('/donasi-sosial/get/data/')
        self.assertEqual(found.func, getData)