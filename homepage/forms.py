from django import forms
from django.forms import ModelForm
from django.forms import fields
from homepage.models import Harapan


forms
class FormHarapan(ModelForm):

    
    isi = forms.CharField(widget=forms.TextInput({'class' : 'form-control','maxlength':'100', 'placeholder':'Saya berharap kedepannya...'}), label='')
    class Meta:
        model = Harapan
        fields = ['isi']
    
        