from django.test import TestCase
from .models import Harapan
from django.test import Client
from django.urls import resolve
from django.urls import reverse
from homepage.views import home
from django.contrib.auth.models import User

# Create your tests here.

class Crowd_fundingUnitTest(TestCase):

    def test_crowd_funding_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_berita_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_crowd_funding_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

class ViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.HarapanUrl = reverse("home:home")
        
    def test_harapan_url_status(self):
        response = self.client.get(self.HarapanUrl)
        self.assertEqual(response.status_code, 200)

    def test_post_tambah_harapan(self):
        user = User.objects.create(username='Tester')
        user.set_password('testing12345')
        user.save()
        self.client.login(username='Tester', password='testing12345')

        response = self.client.post(self.HarapanUrl, {
            "nama":user.username,
            "isi":"Produk yang bagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tester")


#ini buat ngetest str model harapan
#gw kerjain biar coverage ga turun soalnya gw gangerti cara ngetest kerjaan gw heheheee -paris
class ModelTest(TestCase):

    def setUp(self):
        self.harapan = Harapan.objects.create(nama="Budi", isi="Saya suka")

    def test_str(self):
        self.assertEqual(str(self.harapan), "Budi")

    
