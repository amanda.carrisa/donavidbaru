$(document).ready(function(){
    

    $("#id_isi").keyup(function(){
        //character count isi txt area harapan

        var txtlength = $("#id_isi").val().length;
        $("#lettercount").empty();

        if (txtlength > 0) {
            $("#lettercount").append(txtlength+"/100");
        } 
        
        if (txtlength > 85) {
            $("#lettercount").empty();
            $("#lettercount").append('<span style="color:red;">' + txtlength + '/100</span>');
        }
        
    });


    $("#tombol-kirim").click(function(){
        //ajax untuk simpan harapan dan nampilin hope yg baru dimasukkin tsb.

        var isi = $("#id_isi").val();
        // alert(isi);
        if (isi.length > 0 && isi != "") {
            $.ajax({url: "hopeajax?q=" + isi, success: function(result){
                console.log(result);
    
                $("#divharapan").empty();
                $("#id_isi").val("");
                $("#lettercount").empty();
    
                $("#divharapan").append("<p class=\"judul-harapan\">harapan</p>")
                $("#divharapan").append('<div class="row" id="kumpulan-harapan"></div>')
                var i;
                for (i=0; i<result.length; i++){
                    var isihope = result[i].fields.isi;
                    var nama = result[i].fields.nama;
                    // alert(isihope + nama);
                    $("#kumpulan-harapan").append('<div class="col-md-4 mb-4"> <div class="card" style="height: 360px;"><div class="card-body"><div class="harapan-textfield"><p style="padding-top: 40px;">' +isihope+ '</p></div><div class="harapan-nama"><p style="right: 0; position: absolute;">-' +nama+ '</p></div></div></div></div>')
                }
            }});
        } else {
            alert("Ayo isi harapanmu dahulu :D Semoga aja terkabulkan");
        }
        
    });
});