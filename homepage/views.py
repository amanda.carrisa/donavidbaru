from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from homepage.forms import FormHarapan
from homepage.models import Harapan
from django.core import serializers
from django.http.response import JsonResponse
import json

# Create your views here.
def home(request):
    username = 'anon'
    if request.user.is_authenticated:
        username = request.user

        
    if request.POST:
        nama = username
        isi = request.POST.get('isi')
        Harapan.objects.create(nama=nama, isi=isi)
        return HttpResponseRedirect('/')
    else:
        form = FormHarapan()
        harapans = Harapan.objects.all()

        for harapan in harapans:
            print(harapan)

        context = {
            'harapans':harapans,
            'form':form
        }
        return render(request, 'home.html', context)

def hopeajax(request):
    argument = request.GET['q']
    username = request.user
    Harapan.objects.create(nama=username, isi=argument)
    datastr = serializers.serialize("json", Harapan.objects.all())
    data = json.loads(datastr)

    return JsonResponse(data, safe=False)
    