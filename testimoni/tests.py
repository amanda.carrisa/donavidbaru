from django.test import TestCase
from .models import Testimoni
from . import views
from django.test import Client
from django.urls import reverse
from django.contrib.auth.models import User

# Create your tests here.
class ModelTest(TestCase):

    def setUp(self):
        self.testimoni = Testimoni.objects.create(nama="Budi", isi="Saya suka")

    def test_instance_created(self):
        self.assertEqual(Testimoni.objects.count(), 1)

    def test_str(self):
        self.assertEqual(str(self.testimoni), "Budi")

    def test_database_field(self):
        self.assertEqual(self.testimoni.isi, "Saya suka")

class ViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.testimoni = Testimoni.objects.create(nama="Budiman", isi="Saya tidak suka")
        self.testimoniUrl = reverse("testimoni:testimoni")
        
    def test_testimoni_url_status(self):
        response = self.client.get(self.testimoniUrl)
        self.assertEqual(response.status_code, 200)

    def test_post_tambah_testimoni(self):
        user = User.objects.create(username='Tester')
        user.set_password('testing12345')
        user.save()
        self.client.login(username='Tester', password='testing12345')

        response = self.client.post(self.testimoniUrl, {
            "nama":user.username,
            "isi":"Produk yang bagus"
        }, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tester")

    # def test_ajax(self): 

    #     response = self.client.get('/testiajax')

    #     self.assertJSONEqual(
    #         str(response.content, encoding='utf8'),
    #         {'status': 'success'}
    #     )
    

