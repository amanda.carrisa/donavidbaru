from django.urls import path
from . import views

app_name = 'testimoni'

urlpatterns = [
    path('', views.testimoni, name='testimoni'),
    path('testiajax/', views.testiajax, name='testiajax'),
]