$(document).ready(function(){
    

    $("#id_isi").keyup(function(){
        //character count isi txt area testimoni

        var txtlength = $("#id_isi").val().length;
        $("#lettercount").empty();

        if (txtlength > 0) {
            $("#lettercount").append(txtlength+"/100");
        } 
        
        if (txtlength > 85) {
            $("#lettercount").empty();
            $("#lettercount").append('<span style="color:red;">' + txtlength + '/100</span>');
        }
        
    });


    $("#tombol-kirim").click(function(){
        //ajax untuk simpan testimoni dan nampilin testi yg baru dimasukkin tsb.

        var isi = $("#id_isi").val();
        // alert(isi);
        if (isi.length > 0 && isi != "") {
            $.ajax({url: "testiajax?q=" + isi, success: function(result){
                console.log(result);
    
                $("#divtestimoni").empty();
                $("#id_isi").val("");
                $("#lettercount").empty();
    
                $("#divtestimoni").append("<p class=\"judul-testimoni\">Testimoni</p>")
                $("#divtestimoni").append('<div class="row" id="kumpulan-testimoni"></div>')
                var i;
                for (i=0; i<result.length; i++){
                    var isiTesti = result[i].fields.isi;
                    var nama = result[i].fields.nama;
                    // alert(isiTesti + nama);
                    $("#kumpulan-testimoni").append('<div class="col-md-4 mb-4"> <div class="card" style="height: 360px;"><div class="card-body"><div class="testimoni-textfield"><p style="padding-top: 40px;">' +isiTesti+ '</p></div><div class="testimoni-nama"><p style="right: 0; position: absolute;">-' +nama+ '</p></div></div></div></div>')
                }
            }});
        } else {
            alert("Anda belum mengisi testimoni!");
        }
        
    });
});