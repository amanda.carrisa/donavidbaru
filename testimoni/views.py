from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from testimoni.forms import FormTestimoni
from testimoni.models import Testimoni
from django.core import serializers
from django.http.response import JsonResponse
import json

# Create your views here.
def testimoni(request):
    username = 'anon'
    if request.user.is_authenticated:
        username = request.user
        # print(username)

    if request.POST:
        nama = username
        isi = request.POST.get('isi')
        Testimoni.objects.create(nama=nama, isi=isi)
        return HttpResponseRedirect('/testimoni/')
    else:
        form = FormTestimoni()
        testimonis = Testimoni.objects.all()

        for testimoni in testimonis:
            print(testimoni)
        
        context = {
            'testimonis':testimonis,
            'form':form
        }
        return render(request, 'testimoni.html', context)

def testiajax(request):
    argument = request.GET['q']
    username = request.user
    Testimoni.objects.create(nama=username, isi=argument)
    datastr = serializers.serialize("json", Testimoni.objects.all())
    data = json.loads(datastr)

    return JsonResponse(data, safe=False)
    