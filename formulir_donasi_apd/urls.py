from django.contrib import admin
from django.urls import path, include
from . import views 

urlpatterns = [
    path('<str:program>', views.formulir, name='formulir_donasi_apd'),
    path('donasi_apd_berhasil/', views.berhasil, name='donasi_apd_berhasil'),
]
