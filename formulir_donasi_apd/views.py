from django.shortcuts import render, redirect
from .forms import FormDonasiAPD
from .models import DonasiAPD
from donasi_apd.models import Donasiapd

# Create your views here.
def formulir(request, program):
    form = FormDonasiAPD(request.POST or None)
    nama_program = Donasiapd.objects.get(pk=program).name
    foto = Donasiapd.objects.get(pk=program).photo
    program2 = Donasiapd.objects.get(pk=program)
    donasi2 = Donasiapd.objects.all()
    response = {
        'nama_program' : nama_program,
        'donasi2': donasi2,
        'program2': program2,
        'foto': foto
        }
    response["form"] = form
    
    username = 'anonymous'
    if request.user.is_authenticated:
            username = request.user
            alamatEmail = request.user.email

    if(request.method == "POST" and form.is_valid()):
        nama = username
        email = alamatEmail

        
        jmldonasi = request.POST.get("jmldonasi")
        is_anon = request.POST.get("is_anon")
        if is_anon:
            program2.donatur.add(DonasiAPD.objects.create(nama="anonim", email="anonim", jmldonasi=jmldonasi, is_anon=True))
        else:
            program2.donatur.add(DonasiAPD.objects.create(nama=nama, email=email, jmldonasi=jmldonasi, is_anon=False))
        return redirect('donasi_apd_berhasil')
    else:
        return render(request, 'formulir_donasi_apd.html', response)

def berhasil(request):
    response={}
    return render(request,'donasi_apd_berhasil.html',response)
