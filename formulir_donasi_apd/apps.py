from django.apps import AppConfig


class FormulirDonasiApdConfig(AppConfig):
    name = 'formulir_donasi_apd'
