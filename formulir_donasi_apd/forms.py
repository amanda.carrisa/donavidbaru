from django import forms

class FormDonasiAPD(forms.Form):
    # nama = forms.CharField(label="Nama", required=False)
    # email = forms.EmailField(label="Email")
    jmldonasi = forms.FloatField(label="Jumlah Donasi", required=True, min_value=1)
    is_anon = forms.BooleanField(widget=forms.CheckboxInput, required=False, label="Donasi sebagai anonim")

    def clean(self):
        cleaned_data = super().clean()
        # nama = cleaned_data.get("nama")
        is_anon = cleaned_data.get("is_anon")

        # if not is_anon and not nama:
        #     raise forms.ValidationError(
        #         "Nama kosong"
        #     )

   