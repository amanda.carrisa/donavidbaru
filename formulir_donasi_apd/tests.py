from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.forms import ValidationError
from donasi_apd.models import Donasiapd
from .views import formulir, berhasil
from .models import DonasiAPD
from .forms import FormDonasiAPD
import tempfile
import unittest

# Create your tests here.
class Formulir_Donasi_Test(TestCase):
    def test_formulir_donasi_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Donasiapd.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        target = 2000000,
        remDay = 24)
        response = Client().get('/formulir_donasi_apd/1')
        self.assertEqual(response.status_code, 200)

    def test_formulir_donasi_using_to_do_list_template(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Donasiapd.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        target = 2000000,
        remDay = 24)
        response = Client().get('/formulir_donasi_apd/1')
        self.assertTemplateUsed(response, 'formulir_donasi_apd.html')

    def test_formulir_donasi_using_pendaftaran_func(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Donasiapd.objects.create( 
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        target = 2000000,
        remDay = 24)
        found= resolve('/formulir_donasi_apd/1')
        self.assertEqual(found.func, formulir)

    def test_model_can_create_new_user(self):
        new_user = DonasiAPD.objects.create(nama="me", email="amanda@amanda.com", jmldonasi=12000, is_anon=False)
        counting_all_user = DonasiAPD.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_can_create_anon_user(self):
        new_user = DonasiAPD.objects.create(nama="", email="amanda@amanda.com", jmldonasi=12000, is_anon=True)
        counting_all_user = DonasiAPD.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_model_user_name_empty_error(self):
        new_user = DonasiAPD.objects.create(nama="", email="amanda@amanda.com", jmldonasi=12000, is_anon=False)
        self.assertRaises(ValidationError)

    def test_named_UserForm_valid(self):
        form = FormDonasiAPD(data={'nama': "user",'email': "amanda@amanda.com" , 'jmldonasi': "12000", 'is_anon': False})
        self.assertTrue(form.is_valid())

    def test_anon_UserForm_valid(self):
        form = FormDonasiAPD(data={'nama': "",'email': "amanda@amanda.com" , 'jmldonasi': "12000", 'is_anon': True})
        self.assertTrue(form.is_valid())

    def test_UserForm_invalid(self):
        form = FormDonasiAPD(data={'nama': "",'email': "amanda@amanda.com" , 'jmldonasi': "12000"}) #, 'is_anon': False
        self.assertTrue(form.is_valid())

class Donasi_Berhasil_Test(TestCase):
    def test_donasi_berhasil_url_is_exist(self):
        response = Client().get('/formulir_donasi_apd/donasi_apd_berhasil/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_berhasil_using_to_do_list_template(self):
        response = Client().get('/formulir_donasi_apd/donasi_apd_berhasil/')
        self.assertTemplateUsed(response, 'donasi_apd_berhasil.html')

    def test_donasi_berhasil_using_pendaftaran_func(self):
        found= resolve('/formulir_donasi_apd/donasi_apd_berhasil/')
        self.assertEqual(found.func, berhasil)
