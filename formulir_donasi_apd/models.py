from django.db import models

# Create your models here.
class DonasiAPD(models.Model):
    nama = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=100, null = True)
    jmldonasi = models.IntegerField(null = True)
    is_anon = models.BooleanField(null = True)