from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username

def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile, sender=User)

# https://stackoverflow.com/questions/1910359/creating-a-extended-user-profile

# We have a sender here i.e sender=User and a signal of post_save which says that
# when a User is saved then send this signal and the signal will be recieved by 
# the receiver which here is the create_profile function and the create_profile 
# function takes all of arguments that our post_save signal passed to it, one of which 
# is instance and once is created.