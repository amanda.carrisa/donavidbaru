from django.apps import AppConfig


class FormulirDonasiSosialConfig(AppConfig):
    name = 'formulir_donasi_sosial'
