from django.shortcuts import render, redirect
from .forms import FormDonasi
from .models import Donasi
from donasi_sosial.models import Program

# Create your views here.
def formulir(request, program):
    form = FormDonasi(request.POST or None)
    nama_program = Program.objects.get(pk=program).name
    foto = Program.objects.get(pk=program).photo
    program2 = Program.objects.get(pk=program)
    donasi2 = Donasi.objects.all()
    deskripsi = Program.objects.get(pk=program).desc
    
    response = {
        'nama_program' : nama_program,
        'donasi2': donasi2,
        'program2': program2,
        'foto': foto,
        'deskripsi': deskripsi
        }
    response["form"] = form

    username = 'anonymous'
    if request.user.is_authenticated:
            username = request.user
            alamatEmail = request.user.email

    if(request.method == "POST" and form.is_valid()):
        
        nama = username
        email = alamatEmail
        bank = request.POST.get("bank")
        norekening = request.POST.get("norekening")
        jmldonasi = request.POST.get("jmldonasi")
        is_anon = request.POST.get("is_anon")
        

        if is_anon:
            program2.donatur.add(Donasi.objects.create(nama="anonim", email="anonim", bank=bank, norekening=norekening, jmldonasi=jmldonasi, is_anon=True))
        else:
            program2.donatur.add(Donasi.objects.create(nama=nama, email=email, bank=bank, norekening=norekening, jmldonasi=jmldonasi, is_anon=False))
        return redirect('donasi_berhasil')
    else:
        return render(request, 'formulir_donasi.html', response)

def berhasil(request):
    response={}
    return render(request,'donasi_berhasil.html',response)