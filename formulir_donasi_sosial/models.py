from django.db import models

# Create your models here.
class Donasi(models.Model):
    nama = models.CharField(max_length=200, blank=True)
    email = models.EmailField(max_length=100)
    bank = models.CharField(max_length=200, blank=True)
    norekening = models.CharField(max_length=100, blank= True)
    jmldonasi = models.IntegerField()
    is_anon = models.BooleanField()