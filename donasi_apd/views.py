from django.shortcuts import render, redirect
from .models import Donasiapd

# Create your views here.
#start
def list_program(request):
    programs = Donasiapd.objects.all()
    response = {'programs': programs}
    return render(request, 'donasi_apd.html', response)

def list_donatur(request, program):
    donasi_total = Donasiapd.objects.get(pk=program)
    nama_program = Donasiapd.objects.get(pk=program).name
    response = {
        'donasi_terkumpul': donasi_total,
        'nama_program' : nama_program,
        }
    return render(request, 'list_donatur_apd.html', response)

