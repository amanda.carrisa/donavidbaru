from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Donasiapd
from .views import list_program
import tempfile
import unittest

# Create your tests here.
class Donasi_Test(TestCase):
    def test_donasi_url_is_exist(self):
        response = Client().get('/donasi-apd/')
        self.assertEqual(response.status_code, 200)

    def test_donasi_using_to_do_list_template(self):
        response = Client().get('/donasi-apd/')
        self.assertTemplateUsed(response, 'donasi_apd.html')

    def test_donasi_using_pendaftaran_func(self):
        found= resolve('/donasi-apd/')
        self.assertEqual(found.func, list_program)

    def test_model_can_create_new_program(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        new_program = Donasiapd.objects.create(
        name = "Donasi ke korban terdampak pandemi covid",
        photo = image,
        target = 2000000,
        remDay = 24)
        counting_all_programs = Donasiapd.objects.all().count()
        self.assertEqual(counting_all_programs, 1)
