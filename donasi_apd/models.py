from django.db import models
from formulir_donasi_apd.models import DonasiAPD

# Create your models here.
class Donasiapd(models.Model):
    name   = models.CharField(max_length=200)
    photo  = models.ImageField(upload_to="static/img",null=True)
    target = models.IntegerField(null=True)
    remDay = models.IntegerField(null=True)
    donatur = models.ManyToManyField(DonasiAPD, blank=True)
