from django.contrib import admin
from django.urls import path
from . import views

app_name = 'apd'

urlpatterns = [
    path('', views.list_program, name='list_program'),
    path('<str:program>/', views.list_donatur, name='list_donatur_apd'),
]
